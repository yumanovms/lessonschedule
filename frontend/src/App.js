import React from 'react';
import Header2 from './components/Header/Header';
import Menu from './components/Menu/Menu2.js';
import { Container, Header, List } from "semantic-ui-react";


function App() {
    return (
        <div className="App">
            <Header2 />
            <Menu />

        </div>
    );
}

export default App;