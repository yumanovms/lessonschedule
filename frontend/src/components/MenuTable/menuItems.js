// menuItems.js
const menuItems = [
  { id: 1, text: "Неделя" },
  { id: 2, text: "Понедельник" },
  { id: 3, text: "Вторник" },
  { id: 4, text: "Среда" },
  { id: 5, text: "Четверг" },
  { id: 6, text: "Пятница" },
  { id: 7, text: "Суббота" },
  { id: 8, text: "Воскресенье" }
];

export default menuItems;