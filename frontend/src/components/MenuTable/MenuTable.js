import React, { useState } from 'react';
import './MenuTable.css'; // Импортируем стили
import ScheduleTable from '../ScheduleTable/EditableScheduleTable';
import menuItems from './menuItems'; // Импортируем пункты меню

const Menu = () => {
    const [hoveredItem, setHoveredItem] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);
    const [scheduleData, setScheduleData] = useState(null);
    const [selectedDay, setSelectedDay] = useState(null);



    // // Функция для загрузки данных с сервера
    // const fetchDataFromServer = async (day) => {
    //     try {
    //         const response = await fetch(`API_URL/${day}`); // Замени "API_URL" когда сделаешь серв
    //         const data = await response.json();
    //         setScheduleData(data);
    //     } catch (error) {
    //         console.error('Ошибка при загрузке данных:', `API_URL/${day}`);
    //     }
    // };

    // Функция для имитации загрузки данных с сервера
    const fetchDataFromServer = (day) => {
        // Вместо реального запроса, используем имитированные данные
        const mockData = [
            { id: 1, time: "10:00", lesson: "Петя" },
            { id: 2, time: "12:00", lesson: "Катя" },
            { id: 3, time: "15:00", lesson: "Вова" },
            { id: 4, time: "16:00", lesson: "Вова" },
            { id: 5, time: "17:00", lesson: "Вова" },
        ];
        // Устанавливаем имитированные данные в состояние
        setSelectedDay(day);
        setScheduleData(mockData);
    };

    const handleSaveToServer = (updatedSchedule) => {
        // Здесь должен быть реальный код для отправки данных на сервер
        console.log('Отправляем данные на сервер:', updatedSchedule);

    };

    // функция назначения статуса нажания на пункт меню
    const handleMenuClick = (item) => {
        setSelectedItem(item);
        fetchDataFromServer(item.text.toLowerCase());
    };

    return (
        <div>
            <nav className="navtable">
                <ul className="menutable">
                    {menuItems.map(item => (
                        <li
                            key={item.id}
                            className="menuItem"
                            // назначение статуса наведения курсора на пункт меню
                            onMouseEnter={() => setHoveredItem(item)}
                            onMouseLeave={() => setHoveredItem(null)}
                            onClick={() => handleMenuClick(item)}
                        >
                            <a href={`#${item.text}`} className={`link ${hoveredItem === item ? 'linkHover' : ''} ${selectedItem === item ? 'selectedItem' : ''}`}>{item.text}</a>
                        </li>
                    ))}
                </ul>
            </nav>
            {/*что требуется отобразить при активном статусе пункта меню*/}
            {scheduleData && (
                <div className="schedule">
                   {/*<ScheduleTable day={selectedDay} data={scheduleData} />*/}
                   <ScheduleTable day={selectedDay} initialSchedule={scheduleData} onSave={handleSaveToServer} />

                </div>
            )}
        </div>
    );
}

export default Menu;