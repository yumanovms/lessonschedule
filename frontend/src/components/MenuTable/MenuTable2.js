import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import './MenuTable.css'; // Импортируем стили
import ScheduleTable from '../ScheduleTable/EditableScheduleTable';
import menuItems from './menuItems'; // Импортируем пункты меню

const MenuExampleTabular = () => {
  const [activeItem, setActiveItem] = useState('');
  const [scheduleData, setScheduleData] = useState(null);
  const [selectedDay, setSelectedDay] = useState(null);

  const handleItemClick = (e, { name }) => {
    setActiveItem(name);
    fetchDataFromServer(name); // Вызываем функцию загрузки данных с сервера при нажатии на пункт меню
    console.log('данные с сервера:', name);
  };

    // // Функция для загрузки данных с сервера
    // const fetchDataFromServer = async (day) => {
    //     try {
    //         const response = await fetch(`API_URL/${day}`); // Замени "API_URL" когда сделаешь серв
    //         const data = await response.json();
    //         setScheduleData(data);
    //     } catch (error) {
    //         console.error('Ошибка при загрузке данных:', `API_URL/${day}`);
    //     }
    // };

    // Функция для имитации загрузки данных с сервера
    const fetchDataFromServer = (day) => {
        // Вместо реального запроса, используем имитированные данные
        const mockData = [
            { id: 1, time: "10:00", lesson: "Петя" },
            { id: 2, time: "12:00", lesson: "Катя" },
            { id: 3, time: "13:00", lesson: "Вова" },
            { id: 4, time: "15:00", lesson: "Вова" },
            { id: 5, time: "17:00", lesson: "В123weа" },
        ];
        // Устанавливаем имитированные данные в состояние
        setSelectedDay(day);
        setScheduleData(mockData);
    };

    const handleSaveToServer = (updatedSchedule) => {
        // Здесь должен быть реальный код для отправки данных на сервер
        console.log('Отправляем данные на сервер:', updatedSchedule);

    };




  return (
      <div>
    <Menu tabular>

        {menuItems.map(item => (
          <Menu.Item
              key={item.id}
              name={item.text}
              active={activeItem === item.text}
              onClick={handleItemClick}
          />
                ))}

    </Menu>
         {scheduleData && (
             <div className="schedule">
                 {/*<ScheduleTable day={selectedDay} data={scheduleData} />*/}
                 <ScheduleTable day={selectedDay} initialSchedule={scheduleData} onSave={handleSaveToServer}/>

             </div>
         )}
      </div>
  );
};

export default MenuExampleTabular;