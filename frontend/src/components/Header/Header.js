import React, { useState, useEffect } from 'react';
import "./Header.css"

const Header = () => {
    const [currentTime, setCurrentTime] = useState(new Date());

    useEffect(() => {
        const intervalId = setInterval(() => {
            setCurrentTime(new Date());
        }, 1000); // обновляем каждую секунду

        // Очистка интервала при размонтировании компонента
        return () => clearInterval(intervalId);
    }, []); // пустой массив зависимостей, чтобы useEffect сработал только один раз при монтировании

    const timeOptions = {
            hour: 'numeric',
            minute: 'numeric',
            // second: 'numeric'
        };

    return (
        <header  className="header">
            <h1 className="title">Еженедельник</h1>
            <p className="time">Текущее время: {currentTime.toLocaleTimeString([], timeOptions)}</p>
        </header>
    );

}


export default Header;