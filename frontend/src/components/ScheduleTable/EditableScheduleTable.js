import React, { useState } from 'react';
import './ScheduleTable.css';
import { Button } from 'semantic-ui-react'

const EditableScheduleTable = ({ day, initialSchedule, onSave }) => {
    const [schedule, setSchedule] = useState(initialSchedule);

    const handleEdit = (id, field, value) => {
        const updatedSchedule = schedule.map(item => {
            if (item.id === id) {
                return { ...item, [field]: value };
            }
            return item;
        });
        setSchedule(updatedSchedule);
    };

    const handleSave = () => {
        onSave(schedule); // Отправляем измененные данные на сервер
    };

    return (
        <div>
            <h2>Расписание {day}</h2>
            <table className="schedule-table">
                <thead>
                <tr>
                    <th className="table-time">Время</th>
                    <th>Урок</th>
                </tr>
                </thead>
                <tbody>
                {schedule.map(item => (
                    <tr key={item.id}>
                        <td>
                            {item.time}
                            </td>
                        <td >
                            <input
                                type="text"
                                value={item.lesson}
                                onChange={(e) => handleEdit(item.id, 'lesson', e.target.value)}
                            />
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            <h6></h6>
            <Button onClick={handleSave}>Сохранить</Button>

        </div>
    );
};


export default EditableScheduleTable;