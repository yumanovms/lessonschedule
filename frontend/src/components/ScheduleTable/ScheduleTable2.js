import './ScheduleTable.css'; // Подключаем файл стилей

const ScheduleTable = ({ day, data }) => {
    return (
        <div>
            <h2>Расписание {day}</h2>
            <table className="schedule-table">
                <thead>
                    <tr>
                        <th className="table-time">Время</th>
                        <th>Урок</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((item, index) => (
                        <tr key={item.id} className={index % 2 === 0 ? "even-row" : "odd-row"}>
                            <td>{item.time}</td>
                            <td>{item.lesson}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ScheduleTable;