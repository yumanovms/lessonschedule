import React from 'react';
import Botton from '../Botton'; //подключаем компонент таблицы

const ScheduleTable = () => {
    const schedule = [
        { day: 'Monday', subject: 'Math', time: '09:00 - 10:30' },
        { day: 'Monday', subject: 'English', time: '10:45 - 12:15' },
        { day: 'Tuesday', subject: 'Physics', time: '09:00 - 10:30' },
        { day: 'Tuesday', subject: 'Chemistry', time: '10:45 - 12:15' },
        // Добавьте другие уроки по вашему усмотрению
    ];

    return (
        <table style={styles.table}>
            <thead>
                <tr>
                    <th style={styles.th}>Day</th>
                    <th style={styles.th}>Subject</th>
                    <th style={styles.th}>Time</th>
                </tr>
            </thead>
            <tbody>
                {schedule.map((lesson, index) => (
                    <tr key={index} style={styles.tr}>
                        <td style={styles.td}>{lesson.day}</td>
                        <td style={styles.td}><Botton /></td>
                        <td style={styles.td}>{lesson.time}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

const styles = {
    table: {
        width: '80%',
        margin: '20px auto',
        borderCollapse: 'collapse'
    },
    th: {
        border: '1px solid #ddd',
        padding: '8px',
        backgroundColor: '#f2f2f2'
    },
    tr: {
        '&:nth-child(even)': {
            backgroundColor: '#f9f9f9'
        }
    },
    td: {
        border: '1px solid #ddd',
        padding: '8px',
        textAlign: 'center'
    }
}

export default ScheduleTable;