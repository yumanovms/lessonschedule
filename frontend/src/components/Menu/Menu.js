import React, { useState } from 'react';
import './Menu.css'; // Импортируем стили
import ScheduleTable from '../MenuTable/MenuTable'; //подключаем компонент таблицы



const Menu = () => {
    const [hoveredItem, setHoveredItem] = useState(null);
    const [showSchedule, setShowSchedule] = useState(true);
    const [showSchedule2, setShowSchedule2] = useState(false);

    // функция назначения статуса нажания на пункт home
    const handleMenuClick = (item) => {
        if (item === 'schedule') {
            setShowSchedule(true);
        } else {
            setShowSchedule(false);
        }
        if (item === 'Other1') {
            setShowSchedule2(true);
        } else {
            setShowSchedule2(false);
        }
    };

    return (
        <div>
            <nav className="nav">
                <ul className="menu">
                    <li
                        className="menuItem"
                        // назначение статуса наведения курсора на пункт меню
                        onMouseEnter={() => setHoveredItem('schedule')}
                        onMouseLeave={() => setHoveredItem(null)}
                        onClick={() => handleMenuClick('schedule')}
                    >
                        <a href="#schedule" className={`link ${hoveredItem === 'schedule' ? 'linkHover' : ''}`}>Расписание</a>
                    </li>
                    <li
                        className="menuItem"
                        // назначение статуса наведения курсора на пункт меню
                        onMouseEnter={() => setHoveredItem('Other1')}
                        onMouseLeave={() => setHoveredItem(null)}
                        onClick={() => handleMenuClick('Other1')}
                    >
                        <a href="#Other1" className={`link ${hoveredItem === 'Other1' ? 'linkHover' : ''}`}>Other1</a>
                    </li>
                    <li
                        className="menuItem"
                        // назначение статуса наведения курсора на пункт меню
                        onMouseEnter={() => setHoveredItem('Other2')}
                        onMouseLeave={() => setHoveredItem(null)}
                        onClick={() => handleMenuClick('Other2')}
                    >
                        <a href="#Other2" className={`link ${hoveredItem === 'Other2' ? 'linkHover' : ''}`}>Other2</a>
                    </li>
                </ul>
            </nav>
            {/*что требуется отобразить при активном статусе пункта меню home*/}
            {showSchedule && (
                <div className="schedule">
                   <ScheduleTable />
                </div>
            )}

            {showSchedule2 && (
                <div className="Other1">
                    <h1>Анюта моя любимая любовь!</h1>
                </div>
            )}
        </div>
    );
}

export default Menu;