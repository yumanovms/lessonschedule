import React, { Component } from 'react'
import { MenuItem, Icon, Menu } from 'semantic-ui-react'
import './Menu.css'; // Импортируем стили
import MenuTable from "../MenuTable/MenuTable2";

export default class MenuExampleLabeledIcons extends Component {
  state = { activeItem: 'calendar alternate outline' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })



  render() {
    const { activeItem } = this.state

    return (
        <div>
          <Menu icon='labeled'>
            <MenuItem
              name='calendar alternate outline'
              active={activeItem === 'calendar alternate outline'}
              onClick={this.handleItemClick}
            >
              <Icon name='calendar alternate outline' />
              Рассписание
            </MenuItem>

            <MenuItem
              name='heart'
              active={activeItem === 'heart'}
              onClick={this.handleItemClick}
            >
              <Icon name='heart' />
              Избранное
            </MenuItem>

            <MenuItem
              name='folder outline2'
              active={activeItem === 'folder outline2'}
              onClick={this.handleItemClick}
            >
              <Icon name='folder outline' />
              Другое2
            </MenuItem>
          </Menu>
            {activeItem == 'calendar alternate outline' && (
                <MenuTable />
            )}

            {activeItem == 'heart' && (
                <center>
                  <h1>аывпа</h1>
                </center>

            )}
        </div>


    )
  }
}